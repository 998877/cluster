# Start local

## Build local

```
docker-compose build
```

## Start local compose file

```
docker-compose up --scale ping-service=5 --scale sum-service=2 --scale sum2-service=2
```

# Start Cluster

## Initialize the cluster

```
docker swarm init
```

## List nodes

```
#docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
v8nfyjgu0szx5fdr63bfsnwuq *   blackstar           Ready               Active              Leader              18.06.1-ce
```

## Deploy

```
docker stack deploy --compose-file stack.yml aurelium
```

## Acessing the services

 - http://cluster-ip:8080 -> Frontend
 - http://cluster-ip:9080 -> Deployment visualizer

## Debug

### List Stacks
 
```
#docker stack ls
NAME                SERVICES            ORCHESTRATOR
aurelium            7                   Swarm
```

### List Stack Services
 
```
docker stack services aurelium
ID                  NAME                      MODE                REPLICAS            IMAGE                                                PORTS
6kj0dykyq041        aurelium_balancer         replicated          2/2                 aurelio/cluster/balancer:latest                      *:8080->8080/tcp
aicfa022d56c        aurelium_ping-service     replicated          3/3                 registry.gitlab.com/998877/cluster/ping:latest       
gkvrtm3lqsvs        aurelium_sum-service      replicated          1/1                 registry.gitlab.com/998877/cluster/sum:latest        
qmtxfimw2lz4        aurelium_sum3-service     global              1/1                 registry.gitlab.com/998877/cluster/sum:latest        
r4a03pj3ydgm        aurelium_sum2-service     replicated          1/1                 registry.gitlab.com/998877/cluster/sum:latest        
s3yw0q1jt669        aurelium_visualizer       replicated          1/1                 dockersamples/visualizer:latest                      *:9080->8080/tcp
xtrvr6clo35h        aurelium_static-content   replicated          1/1                 registry.gitlab.com/998877/cluster/frontend:latest   

```

### List Services Tasks

```
docker stack ps aurelium
ID                  NAME                                              IMAGE                                                NODE                DESIRED STATE       CURRENT STATE             ERROR                              PORTS
iw1lgeesn6gw        aurelium_sum3-service.v8nfyjgu0szx5fdr63bfsnwuq   registry.gitlab.com/998877/cluster/sum:latest        blackstar           Running             Running 2 minutes ago                                        
kok40p869lzx        aurelium_sum2-service.1                           registry.gitlab.com/998877/cluster/sum:latest        blackstar           Running             Running 2 minutes ago                                        
mr8n3mwwwkzn        aurelium_sum-service.1                            registry.gitlab.com/998877/cluster/sum:latest        blackstar           Running             Running 2 minutes ago                                        
tmz4r5dwezox        aurelium_ping-service.1                           registry.gitlab.com/998877/cluster/ping:latest       blackstar           Running             Running 2 minutes ago                                        
ymfdpgwaimxs        aurelium_balancer.1                               aurelio/cluster/balancer:latest                      blackstar           Running             Running 2 minutes ago                                        
lf48y3ryv73d        aurelium_static-content.1                         registry.gitlab.com/998877/cluster/frontend:latest   blackstar           Running             Running 2 minutes ago                                        
af0wa6wzr5np        aurelium_balancer.1                               aurelio/cluster/balancer:latest                      blackstar           Shutdown            Failed 2 minutes ago      "task: non-zero exit (1)"          
k7ykehp7qdwd        aurelium_visualizer.1                             dockersamples/visualizer:latest                      blackstar           Running             Running 2 minutes ago                                        
z5baayeyd8r9        kfma1y59ji6dmah31lu3nkyqt.1                       manomarks/visualizer:latest                          blackstar           Running             Rejected 13 minutes ago   "No such image: manomarks/visu…"   
hkno2urp5c6t        aurelium_balancer.2                               aurelio/cluster/balancer:latest                      blackstar           Running             Running 2 minutes ago                                        
jdzekethmmgp         \_ aurelium_balancer.2                           aurelio/cluster/balancer:latest                      blackstar           Shutdown            Failed 2 minutes ago      "task: non-zero exit (1)"          
lpck2pmxb3ij        aurelium_ping-service.2                           registry.gitlab.com/998877/cluster/ping:latest       blackstar           Running             Running 2 minutes ago                                        
5wo6id6qbe9k        aurelium_balancer.2                               aurelio/cluster/balancer:latest                      blackstar           Shutdown            Failed 2 minutes ago      "task: non-zero exit (1)"          
tace483wyvg0        aurelium_ping-service.3                           registry.gitlab.com/998877/cluster/ping:latest       blackstar           Running             Running 2 minutes ago      
```

### Show Service logs

```
#docker service logs aurelium_ping-service
aurelium_ping-service.2.lpck2pmxb3ij@blackstar    |  * Serving Flask app "server" (lazy loading)
aurelium_ping-service.2.lpck2pmxb3ij@blackstar    |  * Environment: production
aurelium_ping-service.1.tmz4r5dwezox@blackstar    |  * Serving Flask app "server" (lazy loading)
aurelium_ping-service.3.tace483wyvg0@blackstar    |  * Serving Flask app "server" (lazy loading)
aurelium_ping-service.2.lpck2pmxb3ij@blackstar    |    WARNING: Do not use the development server in a production environment.
aurelium_ping-service.1.tmz4r5dwezox@blackstar    |  * Environment: production
aurelium_ping-service.1.tmz4r5dwezox@blackstar    |    WARNING: Do not use the development server in a production environment.
aurelium_ping-service.3.tace483wyvg0@blackstar    |  * Environment: production
aurelium_ping-service.2.lpck2pmxb3ij@blackstar    |    Use a production WSGI server instead.
aurelium_ping-service.2.lpck2pmxb3ij@blackstar    |  * Debug mode: on
aurelium_ping-service.1.tmz4r5dwezox@blackstar    |    Use a production WSGI server instead.
aurelium_ping-service.3.tace483wyvg0@blackstar    |    WARNING: Do not use the development server in a production environment.
aurelium_ping-service.3.tace483wyvg0@blackstar    |    Use a production WSGI server instead.
aurelium_ping-service.2.lpck2pmxb3ij@blackstar    |  * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
aurelium_ping-service.1.tmz4r5dwezox@blackstar    |  * Debug mode: on
aurelium_ping-service.1.tmz4r5dwezox@blackstar    |  * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
aurelium_ping-service.3.tace483wyvg0@blackstar    |  * Debug mode: on
aurelium_ping-service.2.lpck2pmxb3ij@blackstar    |  * Restarting with stat
aurelium_ping-service.2.lpck2pmxb3ij@blackstar    |  * Debugger is active!
aurelium_ping-service.1.tmz4r5dwezox@blackstar    |  * Restarting with stat
aurelium_ping-service.3.tace483wyvg0@blackstar    |  * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
aurelium_ping-service.3.tace483wyvg0@blackstar    |  * Restarting with stat
aurelium_ping-service.2.lpck2pmxb3ij@blackstar    |  * Debugger PIN: 252-090-659
aurelium_ping-service.1.tmz4r5dwezox@blackstar    |  * Debugger is active!
aurelium_ping-service.1.tmz4r5dwezox@blackstar    |  * Debugger PIN: 185-799-597
aurelium_ping-service.3.tace483wyvg0@blackstar    |  * Debugger is active!
aurelium_ping-service.3.tace483wyvg0@blackstar    |  * Debugger PIN: 148-497-280
aurelium_ping-service.1.tmz4r5dwezox@blackstar    | 10.0.8.2 - - [18/Oct/2018 12:39:47] "GET /api/ping HTTP/1.0" 200 -
aurelium_ping-service.2.lpck2pmxb3ij@blackstar    | 10.0.8.2 - - [18/Oct/2018 12:39:47] "GET /api/ping HTTP/1.0" 200 -
aurelium_ping-service.1.tmz4r5dwezox@blackstar    | 10.0.8.2 - - [18/Oct/2018 12:39:48] "GET /api/ping HTTP/1.0" 200 -
aurelium_ping-service.3.tace483wyvg0@blackstar    | 10.0.8.2 - - [18/Oct/2018 12:39:46] "GET /api/ping HTTP/1.0" 200 -
aurelium_ping-service.3.tace483wyvg0@blackstar    | 10.0.8.2 - - [18/Oct/2018 12:39:47] "GET /api/ping HTTP/1.0" 200 -
aurelium_ping-service.2.lpck2pmxb3ij@blackstar    | 10.0.8.2 - - [18/Oct/2018 12:39:47] "GET /api/ping HTTP/1.0" 200 -
```

## Scale

### Current state

```
#docker stack services aurelium
ID                  NAME                      MODE                REPLICAS            IMAGE                                                PORTS
6kj0dykyq041        aurelium_balancer         replicated          2/2                 aurelio/cluster/balancer:latest                      *:8080->8080/tcp
aicfa022d56c        aurelium_ping-service     replicated          3/3                 registry.gitlab.com/998877/cluster/ping:latest       
gkvrtm3lqsvs        aurelium_sum-service      replicated          1/1                 registry.gitlab.com/998877/cluster/sum:latest        
qmtxfimw2lz4        aurelium_sum3-service     global              1/1                 registry.gitlab.com/998877/cluster/sum:latest        
r4a03pj3ydgm        aurelium_sum2-service     replicated          1/1                 registry.gitlab.com/998877/cluster/sum:latest        
s3yw0q1jt669        aurelium_visualizer       replicated          1/1                 dockersamples/visualizer:latest                      *:9080->8080/tcp
xtrvr6clo35h        aurelium_static-content   replicated          1/1                 registry.gitlab.com/998877/cluster/frontend:latest   

```

### Scaling a service

```
#docker service scale aurelium_ping-service=10 aurelium_sum2-service=10
aurelium_ping-service scaled to 10
aurelium_sum2-service scaled to 10
overall progress: 10 out of 10 tasks 
1/10: running   [==================================================>] 
2/10: running   [==================================================>] 
3/10: running   [==================================================>] 
4/10: running   [==================================================>] 
5/10: running   [==================================================>] 
6/10: running   [==================================================>] 
7/10: running   [==================================================>] 
8/10: running   [==================================================>] 
9/10: running   [==================================================>] 
10/10: running   [==================================================>] 
verify: Service converged 
overall progress: 10 out of 10 tasks 
1/10: running   [==================================================>] 
2/10: running   [==================================================>] 
3/10: running   [==================================================>] 
4/10: running   [==================================================>] 
5/10: running   [==================================================>] 
6/10: running   [==================================================>] 
7/10: running   [==================================================>] 
8/10: running   [==================================================>] 
9/10: running   [==================================================>] 
10/10: running   [==================================================>] 
verify: Service converged 

```

## Shutdown

```
docker stack rm aurelium
docker swarm leave --force
```

# Kubernetes

## Start a local Kubernetes cluster

```
wget https://cdn.rawgit.com/kubernetes-sigs/kubeadm-dind-cluster/master/fixed/dind-cluster-v1.8.sh
chmod +x dind-cluster-v1.8.sh
./dind-cluster-v1.8.sh up
export PATH="$HOME/.kubeadm-dind-cluster:$PATH"
```

## Access via port forward

```
kubectl port-forward service/balancer 8080:8080
```
