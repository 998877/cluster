import requests
import os
import socket
from flask import Flask, request, jsonify

app = Flask(__name__)
hostname = socket.gethostname()
API_NEXT = os.getenv("API", "http://localhost:5000/api/sum")

@app.route("/api/sum", methods=["POST"])
def add_message():
    content = request.json
    result = {}
    result["sum"] = 0
    result["log"] = []
    print("Processing at "+socket.gethostname())
    if content is None:
        return jsonify(result)

    numbers = content["numbers"]
    current = numbers[0]
    print("Received " + str(numbers))
    if len(numbers) == 1:
        result["sum"] = current
    else:
        pending = numbers[1:]
        res = requests.post(API_NEXT, json={"numbers": pending})
        if not res.ok:
            return jsonify(result)
        remoteProcessed = res.json()
        result["sum"] = current + remoteProcessed["sum"]
        result["log"].extend(remoteProcessed["log"])

    result["log"].append("Processing at " + hostname +" result="+str(result["sum"]) + " received="+str(numbers))
    return jsonify(result)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
