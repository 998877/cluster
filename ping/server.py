import socket
from flask import Flask, jsonify

app = Flask(__name__)
hostname = socket.gethostname()


@app.route("/api/ping", methods=["GET"])
def add_message():
    result = {}
    print("Processing at "+socket.gethostname())
    result["log"] = ["Processed at " + hostname]
    return jsonify(result)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
